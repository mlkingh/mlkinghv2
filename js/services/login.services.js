'use strict';

/** Services **/

(function(){
angular.module('mlkingh')

.factory('authFactory', ['$firebaseAuth','FIREBASE_URL', function($firebaseAuth,FIREBASE_URL){
                var ref = new Firebase(FIREBASE_URL);
                var auth = $firebaseAuth(ref);

                return auth;
}]);
})();
