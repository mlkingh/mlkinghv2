'use strict';

 (function(){
angular.module('mlkingh', [
        'ngAnimate',
        'firebase',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'angularUtils.directives.dirPagination'
])

.constant('FIREBASE_URL','https://mlkingh.firebaseio.com/')

.run(["$rootScope", "$location", function($rootScope, $location) {
        $rootScope.$on("$routeChangeError", function(event, next, previous, error) {
          // We can catch the error thrown when the $requireAuth promise is rejected
          // and redirect the user back to the home page
          if (error === "AUTH_REQUIRED") {
            $location.path("/login");
          }
        });
}]);
})();
