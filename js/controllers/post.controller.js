'use strict';

/**
 * Created by Martin on 10/26/2014.
 */
(function(){
angular.module('mlkingh')

/****firebase controllers****/

.controller('postCtrl', ['$scope','Posts', '$routeParams', function($scope, Posts, $routeParams){

    $scope.post = Posts.get($routeParams.postId);
    $scope.posts = Posts.all;
    $scope.addPost = function(post){
		$scope.posts.$add(post)
	},

	$scope._Index = 0;

      // if a current image is the same as requested image
      $scope.isActive = function (index) {
        return $scope._Index === index;
      };

      // show prev image
      $scope.showPrev = function () {
        $scope._Index = Math.max(0, $scope._Index - 1);
      };

      // show next image
      $scope.showNext = function () {
        $scope._Index = Math.min(4, $scope._Index + 1);
      };

      // show a certain image
      $scope.showPhoto = function (index) {
        $scope._Index = index;
      };
}])

.controller('techpostCtrl', ['$scope','TechPosts', '$routeParams', function($scope, TechPosts,$routeParams){

    $scope.techpost = TechPosts.get($routeParams.techpostId);
    $scope.techposts = TechPosts.all;
    $scope.addTechPost = function(techpost){
		$scope.techposts.$add(techpost)
	},

	$scope._Index = 0;

      // if a current image is the same as requested image
      $scope.isActive = function (index) {
        return $scope._Index === index;
      };

      // show prev image
      $scope.showPrev = function () {
        $scope._Index = Math.max(0,$scope._Index - 1);
      };

      // show next image
      $scope.showNext = function () {
        $scope._Index = Math.min(4,$scope._Index + 1);
      };

      // show a certain image
      $scope.showPhoto = function (index) {
        $scope._Index = index;
      };

}])

.controller('travelpostCtrl', ['$scope','TravelPosts', '$routeParams', function($scope, TravelPosts,$routeParams){

       $scope.travelpost = TravelPosts.get($routeParams.travelpostId);
    $scope.travelposts = TravelPosts.all;
    $scope.addTravelPost = function(travelpost){
		$scope.travelposts.$add(travelpost)
	},

	$scope._Index = 0;

      // if a current image is the same as requested image
      $scope.isActive = function (index) {
        return $scope._Index === index;
      };

      // show prev image
      $scope.showPrev = function () {
        $scope._Index = Math.max(0, $scope._Index - 1);
      };

      // show next image
      $scope.showNext = function () {
        $scope._Index = Math.min(4, $scope._Index + 1);
      };

      // show a certain image
      $scope.showPhoto = function (index) {
        $scope._Index = index;
      };

}])

.controller('foodpostCtrl', ['$scope','FoodPosts', '$routeParams', function($scope, FoodPosts, $routeParams){

       $scope.foodpost = FoodPosts.get($routeParams.foodpostId);
    $scope.foodposts = FoodPosts.all;
    $scope.addFoodPost = function(foodpost){
		$scope.foodposts.$add(foodpost)
	},

	$scope._Index = 0;

      // if a current image is the same as requested image
      $scope.isActive = function (index) {
        return $scope._Index === index;
      };

      // show prev image
      $scope.showPrev = function () {
        $scope._Index = Math.max(0, $scope._Index - 1);
      };

      // show next image
      $scope.showNext = function () {
        $scope._Index = Math.min(4, $scope._Index + 1);
      };

      // show a certain image
      $scope.showPhoto = function (index) {
        $scope._Index = index;
      };

}])


/*** blog controllers from local JSON***/

.controller('blogCtrl', ['$scope', 'Article',
    function($scope, Article){
           $scope.articles = Article.query();
}])

.controller('blogpageCtrl', ['$scope','$routeParams','Article',
    function($scope, $routeParams, Article){
      $scope.article = Article.get({articleId: $routeParams.articleId}, function(article){

      $scope.images = [];
      $scope.currentimg = 0;

      // if a current image is the same as requested image
      $scope.isActive = function (index) {
        return $scope.currentimg === index;
      };

      // show a certain image
      $scope.showPhoto = function (index) {
        $scope.currentimg = index;
      };

       // show next image
      $scope.showNext = function () {

     $scope.currentimg = ($scope.currentimg < $scope.article.images.length -1) ? ++$scope.currentimg : 0;

    	  //if ($scope.currentimg > -1) {
        	//	$scope.currentimg++;
        	//	}else if($scope.currentimg = 4){
        	//	$scope.currentimg == index;
        	//}
      };

      // show prev image
      $scope.showPrev = function () {

          $scope.currentimg = ($scope.currentimg > 0) ? --$scope.currentimg : $scope.currentimg = Math.max(0, $scope.currentimg - 1);
      };
      });
}]);
})();
