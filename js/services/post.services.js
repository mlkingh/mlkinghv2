'use strict';

/** Services **/

(function(){
angular.module('mlkingh')

.factory('Posts', ['$firebaseArray','FIREBASE_URL', function ($firebaseArray, FIREBASE_URL){
  var ref = new Firebase(FIREBASE_URL);
  var posts = $firebaseArray(ref.child('posts'));

  posts.$loaded().then(function(posts){
  	 });

     return {
        all: posts,
        create: function (post) {
            return posts.$add(post);
        },
        get: function (postId) {
            var post = posts.$getRecord(postId);
            return post;
        }
    };
}])

.factory('TechPosts', ['$firebaseArray','FIREBASE_URL', function ($firebaseArray, FIREBASE_URL){
  var ref = new Firebase(FIREBASE_URL);
  var techposts = $firebaseArray(ref.child('techposts'));

     return {
        all: techposts,
        create: function (techpost) {
            return techposts.$add(techpost);
        },
        get: function (techpostId) {
            var techpost = techposts.$getRecord(techpostId);
            return techpost;
        }
    };
}])

.factory('TravelPosts', ['$firebaseArray','FIREBASE_URL', function ($firebaseArray, FIREBASE_URL){
  var ref = new Firebase(FIREBASE_URL);
  var travelposts = $firebaseArray(ref.child('travelposts'));

     return {
        all: travelposts,
        create: function (travelpost) {
            return travelposts.$add(travelpost);
        },
        get: function (travelpostId) {
            var travelpost = travelposts.$getRecord(travelpostId);
            return travelpost;
        }
    };
}])

.factory('FoodPosts', ['$firebaseArray','FIREBASE_URL', function ($firebaseArray, FIREBASE_URL){
  var ref = new Firebase(FIREBASE_URL);
  var foodposts = $firebaseArray(ref.child('foodposts'));

     return {
        all: foodposts,
        create: function (foodpost) {
            return foodposts.$add(foodpost);
        },
        get: function (foodpostId) {
            var foodpost = foodposts.$getRecord(foodpostId);
            return foodpost;
        }
    };
}])

.factory('Article', ['$resource',
    function ($resource) {
       return $resource('views/:articleId.json', { }, {
      query: {method:'GET', params:{articleId: 'articles'}, isArray:true}
     });
 }]);

})();
