'use strict';

(function(){
  angular.module('mlkingh')

  .config(['$httpProvider', function($httpProvider) {
          $httpProvider.defaults.useXDomain = true;
          delete $httpProvider.defaults.headers.common['X-Requested-With, Content-Type'];
      }
  ])

  .config(['$routeProvider',
        function($routeProvider) {
          $routeProvider

              .when('/', {
                  templateUrl : 'views/home.html',
                  controller  : 'homeCtrl'
              })

              .when('/myposts', {
                  templateUrl : 'views/mypostpage.html',
                  controller  : 'postCtrl'
              })

              .when('/mytech', {
                  templateUrl : 'views/mytechpage.html',
                  controller  : 'techpostCtrl'
              })

               .when('/mytravel', {
                  templateUrl : 'views/mytravelpage.html',
                  controller  : 'travelpostCtrl'
              })

               .when('/myfood', {
                  templateUrl : 'views/myfoodpage.html',
                  controller  : 'foodpostCtrl'
              })

              .when('/views/pages/:articleId', {
               	templateUrl : 'views/blogpage.html',
                	controller  : 'blogpageCtrl'
              })

              .when('/views/post/:postId', {
                  templateUrl : 'views/post/postpage.html',
                  controller  : 'postCtrl'
              })

               .when('/views/travel/:travelpostId', {
                  templateUrl : 'views/travel/travelpostpage.html',
                  controller  : 'travelpostCtrl'
              })

               .when('/views/food/:foodpostId', {
                  templateUrl : 'views/food/foodpostpage.html',
                  controller  : 'foodpostCtrl'
              })


               .when('/views/tech/:techpostId', {
                  templateUrl : 'views/tech/techpostpage.html',
                  controller  : 'techpostCtrl'
              })

              .when('/about', {
                  templateUrl : 'views/about.html',
                  controller  : 'aboutCtrl'
              })

              .when('/service', {
                  templateUrl : 'views/service.html',
                  controller  : 'serviceCtrl'
              })

              .when('/contact', {
                  templateUrl : 'views/contact.html',
                  controller  : 'contactCtrl'
              })

  	       .when('/create',{
                   templateUrl:'views/pages/create.html',
                   controller:'authCtrl',
                   resolve: {
                      "currentAuth": ["authFactory", function(authFactory) {
                        return authFactory.$requireAuth();
                      }]
                    }
              })

            .when('/login', {
          				templateUrl : 'views/pages/login.html',
          				controller  : 'authCtrl'
  	   	    })

            .when('/signup', {
          				templateUrl : 'views/pages/signup.html',
          				controller  : 'authCtrl'
  	   	    })

    	      .otherwise({redirectsTo:'/'});
        }]);

})();
