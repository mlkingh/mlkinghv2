'use strict';

(function(){
angular.module('mlkingh')

/*** paragraph controllers ***/

.controller('homeCtrl', function($scope) {
  $scope.message = '';
})

.controller('aboutCtrl', function($scope){
  $scope.image = 'https://s3.amazonaws.com/mlkinghweb/images/meban.jpg';
  $scope.message = 'who am I?';
  $scope.content = 'i am a musician, a consultant, a coder, a husband, and a person of faith. this blog is an expression of my life through the lens of my experiences. i love to document my daily encounters, interactions, and observations in pictures. All pictures on this blog were taken with either a Samsung phone/tablet or an Apple ipad mini.'
})

.controller('serviceCtrl', function($scope) {
  $scope.image = 'https://s3.amazonaws.com/mlkinghweb/images/ipad.jpg';
  $scope.message = 'i like to code really cool stuff';
  $scope.content = 'I am a digital business integration  consultant in the Mobility Practice of Accenture Digital. I have over 16 years experience in the tech industry. I specialize in hybrid mobile application architecture, project management and development using HTML5, CSS3, SASS, Ionic Framework, Framework 7, Javascript Libraries, Cordova/Phonegap, BaaS, JSON, RESTful APIs, and cloud solutions. I use modern open source tools such as brackets, yeoman, npm(nodejs), bower, grunt, gulp and github along with Agile Methodologies to guide product development. I love finding digital solutions that help companies to innovate and expand their brand. I am a proud Texas A&M Aggie and a proud nerd!'
})

.controller('contactCtrl', function($scope) {
  $scope.image = 'https://s3.amazonaws.com/mlkinghweb/images/contact.jpg';
  $scope.message = 'start a conversation...five 1 two 5 nine 0 nine 7 two 5';
})

.controller('createCtrl', function($scope) {
  $scope.message = 'Create a post';
});

})();
