'use strict';

(function(){
angular.module('mlkingh')

/****login controller ****/

.controller('authCtrl',['authFactory','$location', '$scope',  function(authFactory ,$location, $scope){

        $scope.user = {
            email: ' ',
            password: ' '
          };

        $scope.login = function (){
           $scope.dataLoading = true;
            authFactory.$authWithPassword($scope.user).then(function (auth){
                $location.path('/create');
            }, function (error){
               $scope.error = error;
               $scope.dataLoading = false;
            });
          };

        $scope.register = function (){
           $scope.dataLoading = true;
            authFactory.$createUser($scope.user).then(function (user){
              $scope.login();
            }, function (error){
              $scope.error = error;
              $scope.dataLoading = false;
            });
          };

          $scope.logout = function(){
            authFactory.$unauth();
            $location.path('/login');
            if ($location.path() === '/create'  && auth.uid === null) {
                $location.path('/login');
            };
          };
}]);
})();
